public class Board {
	private Tile[][] grid;
	private final int gameLength = 3;

	public Board() {
		this.grid = new Tile[this.gameLength][this.gameLength];
		
		//every value is initialized to blank in constructor
		for(int j = 0; j < grid.length; j++) {
			for(int i = 0; i < grid[j].length; i++) {
				grid[j][i] = Tile.BLANK;
			}
		}
	}

	public String toString() {
		//printing board
		String all = "";
		for(int j = 0; j < grid.length; j++) {
			for(int i = 0; i < grid[j].length; i++) {
				all += grid[j][i].getName() + " ";
			}
			all += "\n";
		}
		return all;
	}
	
	public boolean placeToken(int row, int col, Tile playerToken) {
		//false is returned if col or row values are not within board length
		if(row < 0 || row > gameLength-1 || col < 0 || col > gameLength-1){
			return false;
		}
		
		else if(grid[row][col].equals(Tile.BLANK)){
			grid[row][col] = playerToken;
			return true;
		}
		return false;
	}
	public boolean checkIfFull(){
		//if any value is still blank, method returns false
		for(int j = 0; j < grid.length; j++) {
			for(int i = 0; i < grid[j].length; i++) {
				if(grid[j][i] == Tile.BLANK)
					return false;
			}
		}
		return true;
	}
	private boolean checkIfWinningHorizontal(Tile playerToken) {
		for(int j = 0; j < grid.length; j++) {
			boolean allTrue = true;
			if(grid[j][0] == playerToken) {
				for(int i = 1; i < grid[j].length; i++) {
					if(grid[j][i] != grid[j][0]){
							allTrue = false;
							break;
					}
				}
				if(allTrue) {
					return true;
				}
			}
		}
		return false;
	}
	private boolean checkIfWinningVertical(Tile playerToken){
		
		for(int j = 0; j < grid.length; j++){
			boolean allTrue = true;
			if(grid[0][j] == playerToken){
				for(int i = 1; i < grid.length; i++){
					if(grid[i][j] != grid[0][j]){
						allTrue = false;
						break;
					}
				}
				if(allTrue) {
					return true;
				}
			}
		}
		return false;
	}
	private boolean checkIfWinningDiagonal(Tile playerToken){
		boolean allTrueTopDiagonal = true;
		boolean allTrueBottomDiagonal = true;
		if(grid[0][0] == playerToken){
			for(int i = 1; i < grid.length; i++){
				if(grid[i][i] != playerToken){
					allTrueTopDiagonal = false;
					break;
				}
			}
			if(allTrueTopDiagonal) {
				return true;
			}
		}
		else if(grid[grid.length-1][0] == playerToken){
			for(int i = 1; i < grid.length; i++){
				if(grid[grid.length-1-i][i] != playerToken){
					allTrueBottomDiagonal = false;
					break;
				}
			}
			if(allTrueBottomDiagonal){
				return true;
			}
		}

		return false;
	}
	
	public boolean checkIfWinning(Tile playerToken){
		//uses previous checking methods to see if a player has won
		return (checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken) || checkIfWinningDiagonal(playerToken));
	}
}