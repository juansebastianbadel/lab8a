import java.util.Scanner;

public class TicTacToeApp{
	public static void main(String args[]) {
		
		System.out.println("Welcome to tic tac toe!");
		//making a board object
		Board board = new Board();
		Scanner reader = new Scanner(System.in);
		
		boolean gameOver = false;
		int player = 1;
		Tile playerToken = Tile.X;
		
		int row = 0;
		int column = 0;
		
		//gameloop that runs until boolean gameOver is set to true
		while(!gameOver){
			System.out.println(board);
			System.out.println("Player " + player + ", select a row and a column");
			
			//values for row and column set to user's input
			row = Integer.parseInt(reader.nextLine())-1;
			column = Integer.parseInt(reader.nextLine())-1;
			
			//giving player corresponding tile
			if(player == 1){
				playerToken = Tile.X;
			}else{
				playerToken = Tile.O;
			}
			
			//while row and col values are outside the board or taken, values will need update
			while(!board.placeToken(row, column, playerToken)){
				System.out.println("Please enter correct values that are blank");
				
				row = Integer.parseInt(reader.nextLine())-1;
				column = Integer.parseInt(reader.nextLine())-1;
			}	
			
			//corresponding win or tie message will print or players will change turns
			if(board.checkIfWinning(playerToken)){
				System.out.println("Player " + player + " has won!!");
				System.out.println(board);
				gameOver = true;
			}else if(board.checkIfFull()){
				System.out.println("It's a tie...");
				System.out.println(board);
				gameOver = true;
			}else{
				player += 1;
				if(player > 2)
					player = 1;
			}
		}
	}
}

