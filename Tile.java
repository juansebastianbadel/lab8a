public enum Tile {
	BLANK("_"),
	X("X"),
	O("O");

	private String name;

	private Tile(String tile) {
		this.name = tile;
	}

	public String getName() {
		return this.name;
	}
}
